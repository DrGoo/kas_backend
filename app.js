const express = require('express');
var session = require('express-session')

const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv/config')

const TWO_HOURS = 1000 * 60 * 60 * 2;

const { 
    PORT = 3000,
    NODE_ENV = 'development',
    
    SESS_SECRET = 'HELLO/secret^^&key!!',
    SESS_NAME = 'sid',
    SESS_LIFETIME = TWO_HOURS
} = process.env;

const IN_PROD = NODE_ENV === 'production'

app.use(bodyParser.json());
app.use(cors());
app.use(session({
    name: SESS_NAME,
    resave: false,
    saveUninitialized: false,
    secret: SESS_SECRET,
    cookie: {
        maxAge: SESS_LIFETIME,
        sameSite: false,
        secure: IN_PROD
    }
}))

//Middlewares
// app.use('/terms', () => {
    // console.log('hello, this is a middleware running')
    // zde dojde k autentizaci uzivatele
    // app.use(auth)
// })


// Import route
const loginRoute = require('./routes/login')
const registerRoute = require('./routes/register')

app.use('/login', loginRoute)
app.use('/register', registerRoute)




//ROUTING
app.get('/', (req,res) => {
    res.send('We are on home')
})


//CONECT TO DB
mongoose.connect(
    process.env.DB_CONNECTION, { useUnifiedTopology: true }, () => {
    console.log('conected do DB')
})

app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`)
});