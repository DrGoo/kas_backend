const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
  xname: {
    type: String,
    required: true
  },
  license: {
    type: String,
    required: true
  }
  // password: {   type: String
  // }
  //
})

module.exports = mongoose.model('User', UserSchema)
