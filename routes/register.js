const express = require('express')

const router = express.Router();
const User = require('../models/User')
require('dotenv/config')

router.get('/', (req, res) => {
    res.send('we are on register');
})

function getUTF8ArrayFromString(str) {
  var utf8 = unescape(encodeURIComponent(str));

  var arr = [];
  for (var i = 0; i < utf8.length; i++) {
      arr.push(utf8.charCodeAt(i));
  }
  return arr
}

function generateSha1(email) { 
  const { SECRET = 'SSEECCRREETT' } = process.env;
        var message = email + SECRET;
        var textAscii = '';
        var byteArray = '';
        var blocks = '';
        var wordsOfBlocks = '';
        var hash = '';
       //Konstanty podle RFC 3174 standardu
       let h0 = padZero(hexToBinary('67452301'), 32); //01100111010001010010001100000001
       let h1 = padZero(hexToBinary('EFCDAB89'), 32); //11101111110011011010101110001001
       let h2 = padZero(hexToBinary('98BADCFE'), 32); //10011000101110101101110011111110
       let h3 = padZero(hexToBinary('10325476'), 32); //00010000001100100101010001110110
       let h4 = padZero(hexToBinary('C3D2E1F0'), 32); //00010000001100100101010001110110
       
       //Nez budu zpracovavat jednotlive znaky vstupního textu, je potreba ho prevest z UTF-16 (UNICODE) na UTF-8
        var utf8Array = getUTF8ArrayFromString(message);

        //doplnění nul do 8 ciferného binárního čísla (prevedeno z dekadickeho)
        var utf8ByteArray = utf8Array
          .map((charNum) => charNum.toString(2))
          .map((num) => padZero(num, 8));

        //pole byte hodnot se hodi do jednoho stringu a na konec se prida 1
        let byteString = utf8ByteArray.join('') + '1';
      
       // kazdy chunk je potreba aby mel 512 bitu, krome posledniho, ten bude mit 448, aby se nakonec
       // hodila hodnota 64 bitu. Pokud delka byteStringu mod 512 neni 448, pridavej nakonec 0. 
       while (byteString.length % 512 !== 448) {
         byteString += '0';
       }
 
       // delka retezce pole s binarnimi hodnotami ascii znaku, 
       const length = utf8ByteArray.join('').length;
       //delka retezce prevedena na bin. cislo
       const binaryLength = length.toString(2);
 
       // maximalni delka retezce sha-1 2^64 - 1, delka se vejde na 64 bitu a pripoji se na konec byteStringu
       byteString += padZero(binaryLength, 64);
       byteString = byteString;
 
       // rozdeleni binaryString do bloku po 512 bitech
       blocks = chunkSubstr(byteString, 512);
 
       //kazdy blok rodelit 512 bitu rodelit po 16x32 bitech
       wordsOfBlocks = blocks.map((block) => chunkSubstr(block, 32));
 
       // pridani novych slov, z 16 na 80 (32-cifernych), aplikace XOR na vsechna (vybrana 4 slova z bloku)
       const words80 = wordsOfBlocks.map((block) => {
     
         for (let i = 16; i <= 79; i++) {
       
           const wordA = block[i - 3]; //vybrana 4 slova
           const wordB = block[i - 8];
           const wordC = block[i - 14];
           const wordD = block[i - 16];
 
           const xorA = xor(wordA, wordB);
           const xorB = xor(xorA, wordC);
           const xorC = xor(xorB, wordD);
 
           //leva rotace k prave o 1 bit 
           const leftRotated = leftRotate(xorC, 1);
           block.push(leftRotated);
         }
         return block;
       });
 
       //bitove operace na uvodni konstanty a bloky slov
       for (let i = 0; i < words80.length; i++) {
         
         let a = h0;
         let b = h1;
         let c = h2;
         let d = h3;
         let e = h4;
 
         for (let j = 0; j < 80; j++) {
           let f;
           let k;
           if (j < 20) {
             const BandC = and(b, c);
             const notB = and(not(b), d);
             f = or(BandC, notB);
             k = '01011010100000100111100110011001';
           }
           else if (j < 40) {
             const BxorC = xor(b, c);
             f = xor(BxorC, d);
             k = '01101110110110011110101110100001';
           }
           else if (j < 60) {
             const BandC = and(b, c);
             const BandD = and(b, d);
             const CandD = and(c, d);
             const BandCorBandD = or(BandC, BandD);
             f = or(BandCorBandD, CandD);
             k = '10001111000110111011110011011100';
           }
           else {
             const BxorC = xor(b, c);
             f = xor(BxorC, d);
             k = '11001010011000101100000111010110';
           }
           //konstansty se priradi a pouziji v dalsim cykly (z 80)
           const word = words80[i][j];
           const tempA = binaryAddition(leftRotate(a, 5), f);
           const tempB = binaryAddition(tempA, e);
           const tempC = binaryAddition(tempB, k);
           let temp = binaryAddition(tempC, word);
 
           temp = truncate(temp, 32);
           e = d;
           d = c;
           c = leftRotate(b, 30);
           b = a;
           a = temp;
         }
 
         //dohromady konstanty a zkraceni na 32 bitu
         h0 = truncate(binaryAddition(h0, a), 32);
         h1 = truncate(binaryAddition(h1, b), 32);
         h2 = truncate(binaryAddition(h2, c), 32);
         h3 = truncate(binaryAddition(h3, d), 32);
         h4 = truncate(binaryAddition(h4, e), 32);
       }
       hash = [h0, h1, h2, h3, h4].map((string) => binaryToHex(string)).join('');
       return hash;
}

function isHash() {
    return hash;
  }

function padZero(num, length) {
    let numArray = num.toString().split('');
    while (numArray.length < length) {
      numArray.unshift('0');
    }

    return numArray.join('');
  }

function leftRotate(string, num) {
    return string.slice(num) + string.slice(0, num);
  }

function binaryToHex(string) {
    if (typeof string !== 'string') string = string.toString();
    let decimal = parseInt(string, 2);
    return decimal.toString(16);
  }

function chunkSubstr(str, size) {
    const numChunks = Math.ceil(str.length / size)
    const chunks = new Array(numChunks)

    for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
      chunks[i] = str.substr(o, size)
    }

    return chunks
  }

function xor(strA, strB) {
    let arrayA = strA.split('').map((letter) => parseInt(letter) );
    let arrayB = strB.split('').map((letter) => parseInt(letter) );
    const xorArray = arrayA.map((num, index) => num ^ arrayB[index]);
    return xorArray.join('').toString();
  }

function and(strA, strB) {
    let arrayA = strA.split('').map((letter) => +letter );
    let arrayB = strB.split('').map((letter) => +letter );
    const andArray = arrayA.map((num, index) => num & arrayB[index]);
    return andArray.join('').toString();
  }

function or(strA, strB) {
    let arrayA = strA.split('').map((letter) => +letter );
    let arrayB = strB.split('').map((letter) => +letter );
    const orArray = arrayA.map((num, index) => num | arrayB[index]);
    return orArray.join('').toString();
  }

function not(strA) {
    let array = strA.split('').map((letter) => letter );
    return array.map(letter => {
      if (letter === '1') return '0';
      return '1';
    }).join('');
  }

function binaryAddition(stringA, stringB) {
    const numA = parseInt(stringA, 2);
    const numB = parseInt(stringB, 2);
    let sum = (numA + numB).toString(2);
    const length = stringA.length;

    while (sum.length < stringA.length) {
      sum = '0' + sum;
    }

    return sum.length === length ? '1' + sum : sum;
  }

function truncate(string, length) {
    while (string.length > length) {
      string = string.slice(1);
    }

    return string;
  }

function hexToBinary(hex) {
    const number = parseInt(hex, 16);
    return (number >>> 0).toString(2); //>>> je shift, cili 0 posuvu doleva
    
  }

router.post('/', async (req, res) => {
    const { userId } = req.session;

    const licensedUser = await User.findOne({ xname: req.body.xname })
    
    if (!licensedUser) {
        // create new licence
        

        const licenseKey = generateSha1(req.body.xname);
        const user = new User({
            xname: req.body.xname,
            license: licenseKey,
        })
        const savedUser = await user.save()

        res.status(200).send(licenseKey)
        // return res.status(400).send('Uživatel nemá platnou licenci')
    } else {
         // the user has already licence
         return res.status(400).send('Uživatel již má platnou licenci')
    }
      
})
module.exports = router;