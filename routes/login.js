const express = require('express')
// const puppeteer = require('puppeteer');
const treekill = require('tree-kill');

const router = express.Router();
const User = require('../models/User')

// const redirectLogin = (req, res, next) => {
//     if (!req.session.userId) {
//         res.redirect('/login')
//     } else {
//         next()
//     }
// }

router.get('/', (req, res) => {
    res.send('we are on login');
})

router.get('/specific', (req, res) => {
    res.send('we are on specific login');
})

router.post('/', async (req, res) => {
    const { userId } = req.session;

    const licensedUser = await User.findOne({ license: req.body.license })
    
    if (!licensedUser) {
        // create new licence
        return res.status(400).send('Neplatná licence')
    } else {
        res.status(200).send({licensed: true});
    }      
})

// const maxRunningTime = 60 * 60 * 1000;

// let browser = null;

// const getActualBrowser = () => {
//     return browser;
// }

// const getBrowser = async() => {
//     if (browser) {
//         return browser;
//     }
//     browser = await puppeteer.launch({
//         headless: false, //false
//         slowMo: 20
//     });
//     browser.__BROWSER_START_TIME_MS__ = Date.now();
//     return browser;
// };

// const cleanup = async(browser) => {
//     // Kill it if it's time
//     if ((Date.now() - browser.__BROWSER_START_TIME_MS__) >= maxRunningTime) {
//         treekill(browser.process().pid, 'SIGKILL');
//         browser = null;
//         await getBrowser();
//     }
//     // Cleanup the browser's pages
//     const pages = await browser.pages();
//     return Promise.all(pages.map((page) => page.close()));
// };

// const createNewSession = async(name) => {
//     const context = await browser.createIncognitoBrowserContext();
//     // Create a new page inside context.
//     const page = await context.newPage();
//     // ... do stuff with page ...
//     await page.goto('https://example.com');
//     // Dispose context once it's no longer needed.
//     await context.close();
// } 

//SUBMIT A USER
// router.post('/', async (req, res) => {
//     const { userId } = req.session;

//     const licensedUser = await User.findOne({ xname: req.body.xname })
    
//     if (!licensedUser) {
//         return res.status(400).send('Uživatel nemá platnou licenci')
//     } else {

//         req.session.userId = licensedUser._id;
//         try {
//             const browser =  await puppeteer.launch({
//                 headless: false, //false
//                 slowMo: 20
//             });
//             const page = await browser.newPage();
//             await page.goto('https://is.mendelu.cz/auth/');
        
//             await page.type('#credential_0', req.body.xname);
//             await page.type('#credential_1', req.body.pass);
//             await page.click('#login-btn');
            
//             await page.waitForNavigation();
//             await page.goto('https://is.mendelu.cz/auth/student/terminy_seznam.pl?lang=cz;', { waitUntil: 'networkidle0' });
            
//             // const appData = await page.$$eval('#content > div.mainpage > form > table:nth-child(14) tbody tr', trs => trs.map((tr) => {
//             const terms = await page.$$eval('#table_2 tbody tr', trs => trs.map((tr) => {
//                 return {
//                 subject: tr.querySelector('td:nth-child(4)').textContent,
//                 name: tr.querySelector('td:nth-child(5)').textContent,
//                 date: tr.querySelector('td:nth-child(7) small').textContent,
//                 entered: tr.querySelector('td:nth-child(8)').textContent,
//                 url: tr.querySelector('td:nth-child(14) small').innerHTML
//                 }
//             }));
            
//             // cleanup(browser);
//             // await browser.disconnect();
//             res.status(200).send(terms)
//             await browser.close();

//         } catch(err) {
//             console.log(err)
//             await browser.close();
//         }
          
//         // browser.disconnect();
//     }
//     // const user = new User({
//     //     xname: req.body.xname
//     // })
//     // try {
//     //     const savedUser = await user.save()
//     //     res.json(savedUser);
//     // } catch(err) {
//     //     res.json({message: err})
//     // }
// })



module.exports = router;